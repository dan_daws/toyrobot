<?php
	require __DIR__ . '/../vendor/autoload.php';
	require __DIR__ . '/../RobotClass.php';
	use PHPUnit\Framework\TestCase;

	class RobotTest extends TestCase
	{
        //Test Basic placement in 0 0
		public function testInitPlacement() {
			$r = new Robot;
			$r->initPlacement(0, 0, "NORTH");
			$this->assertEquals("0,0,NORTH\n", $r->report());
		}

        //Test out of bounds placement
        public function testOutOfBoundsPlacement() {
			$r = new Robot;
			$val = $r->initPlacement(6, 6, "NORTH");
			$this->assertEquals("Coords Out of Limit, will fall off table", $val);
        }

        //Test negative coord initiatlisation
        public function testBadCoord() {
			$r = new Robot;
			$val = $r->initPlacement(-1, -1, "NORTH");
			$this->assertEquals("Negative Coords Unacceptable", $val);
        }

        //Test a standard move
        public function testMove() {
			$r = new Robot;
			$r->initPlacement(0, 0, "NORTH");
            $r->move();
			$this->assertEquals("0,1,NORTH\n", $r->report());
        }

        //test a move out of bounds
        public function testOutOfBoundsMove() {
			$r = new Robot;
			$r->initPlacement(0, 5, "NORTH");
            //Test the move returns Will Fall
			$this->assertEquals("Will Fall", $r->move());
            //Test that the robot remains in the same place
			$this->assertEquals("0,5,NORTH\n", $r->report());
        }

        public function testTurnLeft() {
			$r = new Robot;
			$r->initPlacement(0, 0, "NORTH");

            $r->turn("LEFT");
            //Turning left after facing north should mean the robot faces WEST now
			$this->assertEquals("0,0,WEST\n", $r->report());

            $r->turn("LEFT");
            //Turning left after facing WEST should mean the robot faces SOUTH now
			$this->assertEquals("0,0,SOUTH\n", $r->report());

            $r->turn("LEFT");
            //Turning left after facing SOUTH should mean the robot faces EAST now
			$this->assertEquals("0,0,EAST\n", $r->report());

            $r->turn("LEFT");
            //Turning left after facing EAST should mean the robot faces NORTH now
			$this->assertEquals("0,0,NORTH\n", $r->report());
        }

        public function testTurnRight() {
			$r = new Robot;
			$r->initPlacement(0, 0, "NORTH");

            $r->turn("RIGHT");
            //Turning right after facing north should mean the robot faces EAST now
			$this->assertEquals("0,0,EAST\n", $r->report());

            $r->turn("RIGHT");
            //Turning right after facing EAST should mean the robot faces SOUTH now
			$this->assertEquals("0,0,SOUTH\n", $r->report());

            $r->turn("RIGHT");
            //Turning left after facing SOUTH should mean the robot faces WEST now
			$this->assertEquals("0,0,WEST\n", $r->report());

            $r->turn("RIGHT");
            //Turning left after facing WEST should mean the robot faces NORTH now
			$this->assertEquals("0,0,NORTH\n", $r->report());
        }

        public function testInvalidDirect() {
			$r = new Robot;
			$r->initPlacement(0, 0, "NORTH");
            $this->assertEquals("Invalid Turn Direction", $r->turn("AROUND"));
        }
    }
?>

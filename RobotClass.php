<?php

class Robot {
    //xcoord of position on board
    private $xcoord;
    //ycoord of position on board
    private $ycoord;
    //integer corresponding to 'HEADINGS' array
    private $headingCursor = -1;
    //constant mapping of directions the robot is facing
    const HEADINGS = array('NORTH', 'EAST', 'SOUTH', 'WEST');
    //Definied as constant extensible to a bigger table size if needed
    const xLimit = 5;
    const yLimit = 5;

    public function initPlacement($x, $y, $heading) {
        if($x < 0 || $y < 0) {
            //Negative coordinates don't make sense
            return "Negative Coords Unacceptable";
        }
        if($x > self::xLimit || $y > self::yLimit) {
            //We would start off the table
            return "Coords Out of Limit, will fall off table";
        }
        for($i = 0; $i < count(self::HEADINGS); $i++) {
            if(self::HEADINGS[$i] == $heading) {
                $this->headingCursor = $i;
                break;
            }
        }
        //We didn't get an element defined in HEADINGS for a heading
        if($this->headingCursor < 0) return false;
        $this->xcoord = $x;
        $this->ycoord = $y;
    }

    public function move() {
        switch(self::HEADINGS[$this->headingCursor]) {
            case 'NORTH':
                if($this->ycoord + 1 <= self::yLimit) {
                    $this->ycoord++;
                    break;
                } else {
                    //Ignore as we would fall off the table;
                    return "Will Fall";
                }
            case 'EAST':
                if($this->xcoord + 1 <= self::xLimit) {
                    $this->xcoord++;
                    break;
                } else {
                    //Ignore as we would fall off the table;
                    return "Will Fall";
                }
            case 'SOUTH':
                if($this->ycoord - 1 > 0) {
                    $this->ycoord--;
                    break;
                } else {
                    //Ignore as we would fall off the table;
                    return "Will Fall";
                }
            case 'WEST':
                if($this->xcoord - 1 > 0) {
                    $this->xcoord--;
                    break;
                } else {
                    //Ignore as we would fall off the table;
                    return "Will Fall";
                }
            default:
                //Invalid state
                return "Invalid State"; //could use assertion to fail more gracefully
        }
    }

    public function turn($direction) {
        //If the robot turns right it turns 90 degrees
        //clockwise which means its heading changes to the next element
        //in the 'HEADINGS' array. Instead of a circularly linked list an
        //if conditional is checked for the next element.
        if($direction == 'RIGHT') {
            if($this->headingCursor + 1 > count(self::HEADINGS)-1) {
                $this->headingCursor = 0;
            } else {
                $this->headingCursor++;
            }
        //If the robot turns left it turns 90 degrees
        //anti-clockwise which means its heading changes to the previous element
        //in the 'HEADINGS' array. Instead of a circularly linked list an
        //if conditional is checked for the next element.
        } else if($direction == 'LEFT') {
            if($this->headingCursor - 1 < 0) {
                $this->headingCursor = 3;
            } else {
                $this->headingCursor--;
            }
        }
        return "Invalid Turn Direction";
    }

    //Print out the current state of the robot
    public function report() {
        return $this->xcoord . "," . $this->ycoord . "," . self::HEADINGS[$this->headingCursor] . "\n";
    }
}

?>

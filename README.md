# README #


The application is meant to run via the command line on a unix-like system. After pulling down the repository it can be run by typing:


> 'php main.php testfile.txt'


The above command will execute the application using the data in "testfile.txt" - Ensure that php is installed and that relevant file permissions are correct. 


Usage of PHPUnit tests will require Composer to install the phpUnit dependencies. To install Composer check here: https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx


### Running Unit Tests ###


You must have phpunit installed on your computer to run the test scripts, and you must also have composer install the phpunit depndencies for the project (see above)


If this is installed then from the base directory run:


> 'phpunit tests/testscript.php' 


You should see something like:

> PHPUnit 6.1.4 by Sebastian Bergmann and contributors.


> ........                                                            8 / 8 (100%)


> Time: 62 ms, Memory: 8.00MB


> OK (8 tests, 15 assertions)



### Design Choices ###

The actions of the robot are all definied in a 'robot' class, with the functions for the robot implemented and the state of the robot at any given point 
definied as member variables for the class.

A main.php file is used as an entry point and a simple parser reads in the file name supplied in the CLI arguments. 

### Assumptions ###
It is assumed that the files supplied only have one "PLACE" command and are of the syntax in the specification. While it may still work, results are hard to determine.

### Test Coverage ###

Test coverage only exersizes the functions in the robot class, for both basic usage as well as expected failure usage. It doesn't exersize file parsing although that could
be something covered in the future. 

### Requirements / Spec Push Back ###

Having the commands as 'LEFT', 'RIGHT', and having the heading/facing as "NORTH", "WEST", "SOUTH, "EAST", is not extensible. In a real-world scenario I would suggest using
degrees to measure direction. I.e. LEFT would be -90, RIGHT would be +90 and heading could be expressed as degrees from 0-359, 

### Extensibility ###

* One could build another module for graphically displaying the robots position as it moves to visually see it working.
* One could remove the text file input ,pdi;e and use a real time controller to call the robot class in real time (i.e. like a controller)
<?php
    //Entry Point for Robot Simulator
    require('RobotClass.php');

    if(count($argv) != 2) die("You must provide the filename used, e.g. php main.php testfile.txt");
    readInFile($argv[1]);

    function readInFile($filename) {
        if(!file_exists($filename)) die("File $filename doesn't exist");
        $contents = file_get_contents($filename);
        run($contents);
    }

    //Given parsed in content, this function iterates over it and extracts
    //the commands and runs them on the robot object.
    function run($contents) {
        $contents = trim($contents);
        $r = new Robot;
        $arr = explode(" ", $contents);
        foreach($arr as $key=>$command) {
            switch($command) {
                case "PLACE":
                    $placeData = explode(",", $arr[$key+1]);
                    if(!$placeData) die("PLACE not followed by X,Y,F");
                    $r->initPlacement($placeData[0], $placeData[1], $placeData[2]);
                    break;
                case "MOVE":
                    $r->move();
                    break;
                case "LEFT":
                    $r->turn("LEFT");
                    break;
                case "RIGHT":
                    $r->turn("RIGHT");
                    break;
                case "REPORT":
                    echo $r->report();
                    break;
               default:
                    //If the previous command wasn't PLACE we don't know the command
                    if($arr[$key-1] != "PLACE") {
                        die("Improper command received: $command");
                    }
            }
        }
    }

?>
